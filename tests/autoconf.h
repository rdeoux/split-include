/* Generated configuration */
#ifndef AUTOCONF_H
#define AUTOCONF_H

/* Feature A */
#define CONFIG_FEATUREA_OPTION1 1
#undef CONFIG_FEATUREA_OPTION2

/* Feature B */
#undef CONFIG_FEATUREB_OPTION3
#define CONFIG_FEATUREB_OPTION4 2

#endif /* !defined AUTOCONF_H */
